# Este es el archivo Makefile de la práctica, editarlo como sea neceario

# Target que se ejecutará por defecto con el comando make
programa:

# Target que compilará el proyecto usando librerías estáticas
static:

# Target que compilará el proyecto usando librerías dinámicas
dynamic:

# Limpiar archivos temporales
clean:
